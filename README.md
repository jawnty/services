# Welcome to Jawnty.
A collaboration platform built around pulling cloud and on-premises services into a single user-centered frontend.

## Frontend
```
WIP
```

## Deployment
Jaunty requires ZFS & Docker installed on the host. We also require a ZFS volume driver like the wonderful docker-zfs-plugin from TrilliumIT.

Initial Jaunty Backend configuration requires a list of environment variables injected to the host, either via boot options or the JauntyOS installation process:
```
WIP
```

Deploy the `system` Swarm stack with these environment variables set of the host, and visit admin.yourdomain.org to start up your infrastructure.

Please contact Jawnty for support on deploying services in the cloud or on-premises.

