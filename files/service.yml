version: '3.7'

services:
  cache:
    image: redis:alpine
    environment:
      - COMPANY_DOMAIN
    volumes:
      - cache:/data
    networks:
      backend:
    deploy:
      replicas: 1
      update_config:
        parallelism: 1
        delay: 10s
      placement:
        constraints:
          - node.hostname == core
      restart_policy:
        condition: on-failure
  db:
    image: postgres:alpine
    environment:
      - POSTGRES_PASSWORD
      - POSTGRES_USER
      - POSTGRES_DB
      - COMPANY_DOMAIN
    volumes:
      - db:/var/lib/postgresql/data
    networks:
      backend:
    deploy:
      replicas: 1
      update_config:
        parallelism: 1
        delay: 10s
      placement:
        constraints:
          - node.hostname == core
  app:
    image: registry.gitlab.com/jawnty/services/nextcloud:latest
    hostname: projects
    environment:
      - NEXTCLOUD_ADMIN_USER
      - NEXTCLOUD_ADMIN_PASSWORD
      - NEXTCLOUD_TRUSTED_DOMAINS
      - POSTGRES_PASSWORD
      - POSTGRES_USER
      - POSTGRES_DB_NC
      - REDIS_HOST
      - COMPANY_DOMAIN
    depends_on:
      - db
      - cache
    volumes:
      - html:/var/www/html
      - data:/var/www/html/data
      - config:/var/www/html/config
      - apps:/var/www/html/apps
      - themes:/var/www/html/themes
    networks:
      - backend
    deploy:
      replicas: 1
      update_config:
        parallelism: 1
        delay: 10s
      placement:
        constraints:
          - node.hostname == core
      restart_policy:
        condition: on-failure
  web:
    image: nginx:alpine
    environment:
      - COMPANY_DOMAIN
    depends_on:
      - app
    volumes:
      - html:/var/www/html:ro
      - data:/var/www/html/data:ro
      - config:/var/www/html/config:ro
      - apps:/var/www/html/apps:ro
      - themes:/var/www/html/themes:ro
    configs:
      - source: files_web_nginx
        target: /etc/nginx/nginx.conf
        uid: '0'
        gid: '0'
        mode: 0444
    networks:
      - backend
      - proxy
    deploy:
      labels:
        traefik.enable: 'true'
        traefik.http.middlewares.redirect.redirectscheme.scheme: https
        traefik.http.routers.files_redirect.entrypoints: web
        traefik.http.routers.files_redirect.middlewares: redirect
        traefik.http.routers.files_redirect.rule: 'Host(`files.${COMPANY_DOMAIN}`)'
        traefik.http.routers.files.entrypoints: websecure
        traefik.http.routers.files.tls: 'true'
        traefik.http.routers.files.tls.certresolver: letsencrypt
        traefik.http.routers.files.middlewares: nextcloud,nextcloud_redirect
        traefik.http.routers.files.rule: 'Host(`files.${COMPANY_DOMAIN}`)'
        traefik.http.middlewares.nextcloud.headers.stsSeconds: 155520011
        traefik.http.middlewares.nextcloud_redirect.redirectregex.permanent: 'true'
        traefik.http.middlewares.nextcloud_redirect.redirectregex.regex: "https://(.*)/.well-known/(card|cal)dav"
        traefik.http.middlewares.nextcloud_redirect.redirectregex.replacement: "https://$${1}/remote.php/dav/"
        traefik.http.services.files.loadbalancer.server.port: 80
      replicas: 1
      update_config:
        parallelism: 1
        delay: 10s
      placement:
        constraints:
          - node.hostname == core
      restart_policy:
        condition: on-failure

networks:
  backend:
    driver: overlay
    attachable: true
  proxy:
    driver: overlay
    external: true

configs:
  files_web_nginx:
    file: "./nginx.conf"

volumes:
  html:
    driver: zfs
    name: "${DATA_SERVICES}/files/html"
  data:
    driver: zfs
    name: "${DATA_SERVICES}/files/data"
  config:
    driver: zfs
    name: "${DATA_SERVICES}/files/config"
  apps:
    driver: zfs
    name: "${DATA_SERVICES}/files/apps"
  themes:
    driver: zfs
    name: "${DATA_SERVICES}/files/themes"
  db:
    driver: zfs
    name: "${DATA_SERVICES}/files/db"
    driver_opts:
        recordsize: 8K
  cache:
    driver: zfs
    name: "${DATA_SERVICES}/files/cache"