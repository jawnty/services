version: '3.7'

services:
  app:
    image: requarks/wiki:beta
    restart: always
    depends_on:
      - db
    environment:
      - DB_TYPE
      - DB_HOST
      - DB_PORT
      - DB_USER
      - DB_PASS
      - DB_NAME
      - TZ
      - COMPANY_DOMAIN
    volumes:
      - cfg:/wiki/config
      - repo:/wiki/repo
    networks:
      backend:
      proxy:
    deploy:
      labels:
        traefik.enable: 'true'
        traefik.http.middlewares.redirect.redirectscheme.scheme: https
        traefik.http.routers.wiki-http.entrypoints: web
        traefik.http.routers.wiki-http.middlewares: redirect
        traefik.http.routers.wiki-http.rule: 'Host(`wiki.${COMPANY_DOMAIN}`)'
        traefik.http.routers.wiki.entrypoints: websecure
        traefik.http.routers.wiki.rule: 'Host(`wiki.${COMPANY_DOMAIN}`)'
        traefik.http.routers.wiki.tls: 'true'
        traefik.http.routers.wiki.tls.certresolver: letsencrypt
        traefik.http.services.wiki.loadbalancer.server.port: 3000
      replicas: 1
      update_config:
        parallelism: 1
        delay: 10s
      placement:
        constraints: 
          - node.role == manager
  db:
    image: postgres:9-alpine
    hostname: wiki
    environment:
      - POSTGRES_DB
      - POSTGRES_PASSWORD
      - POSTGRES_USER
    volumes:
      - db:/var/lib/postgresql/data
    networks:
      backend:
    deploy:
      replicas: 1
      update_config:
        parallelism: 1
        delay: 10s
      placement:
        constraints: 
          - node.role == manager

networks:
  backend:
    driver: overlay
  proxy:
    external: true

volumes:
  cfg:
    driver: zfs
    name: "${DATA_SERVICES}/wiki/cfg"
  db:
    driver: zfs
    name: "${DATA_SERVICES}/wiki/db"
    driver_opts:
      recordsize: 64K
  repo:
    driver: zfs
    name: "${DATA_SERVICES}/wiki/repo"